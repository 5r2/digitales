import serial

datos = []

#while True:
with serial.Serial('/dev/ttyACM0', 9600, timeout=1) as ser:
    for i in range(35):
        recib = ser.read(2) #280
        y = recib.decode('ASCII')
        datos.append(hex(int(y, 16)))
    datos.pop(0)
    datos.pop(len(datos)-1)
    datos.pop(int((len(datos)-1)/2))

    real = datos[0:int(len(datos)/2)]
    imag = datos[int(len(datos)/2):len(datos)]

    print(real)
    print(imag)

#print(datos)
