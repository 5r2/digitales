#!/usr/bin/env octave-cli

M = imread('./lena.tiff');
[F, C, cl] = size(M);
imshow(M);pause;

I = M(:,end:-1:1,:);
imshow(I);pause;

J = M(end:-1:1,:,:);
imshow(J);pause;

M_d = double(M);
M_d_av = (M_d(:,:,1) + M_d(:,:,2) + M_d(:,:,3))./3;
M_u8 = uint8(M_d_av);
imshow(M_u8);pause;

M_ojo = M(250:285,245:290,:);
M_ojo_cuadro = M_ojo;

M_ojo_cuadro(1,1:end,1) = 255;
M_ojo_cuadro(1,1:end,2:3) = 0;
M_ojo_cuadro(end,1:end,1) = 255;
M_ojo_cuadro(end,1:end,2:3) = 0;

M_ojo_cuadro(1:end,1,1) = 255;
M_ojo_cuadro(1:end,1,2:3) = 0;
M_ojo_cuadro(1:end,end,1) = 255;
M_ojo_cuadro(1:end,end,2:3) = 0;

imshow(M_ojo_cuadro);pause;

M_mapeo(:,:,1) = M(:,:,2);
M_mapeo(:,:,2) = M(:,:,3);
M_mapeo(:,:,3) = M(:,:,1);
imshow(M_mapeo);pause;

function Q=brillo_contraste(I,B,C)
  I_d = double(I);
  I_d_av = (I_d(:,:,1) + I_d(:,:,2) + I_d(:,:,3))./3;
  I_min = min(min(I_d_av));
  I_max = max(max(I_d_av));

  I_d_c = ( ( ( (I_d.-I_min)./(I_max.-I_min) ).*(C+1) ).+(1-C) ).*128;
  I_d_b = I_d_c.+(255*(2*B-1));

  I_u8 = uint8(I_d_b);

Q = I_u8;
end

M_procesado = brillo_contraste(M,0.2,0.5);
imshow(M_procesado);pause;