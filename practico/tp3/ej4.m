#!/usr/bin/env octave-cli

F = 5
C = 5

N = 5
M = 5

x=rand(F,C)
h=rand(N,M)

y=zeros(F+N-1, C+M-1)

hinv = fliplr(flipud(h))

ind = [1,1]

ampl = zeros(2*N+F-2, 2*M+C-2)

ampl(N:N+F-1, M:M+C-1 ) = x

for i=1:(F+N-1)
  for j=1:(C+M-1)
    pa = ampl(j:(N-1+j),i:(M-1+i))
    p = pa.*hinv
    y(j,i) = sum(sum(p))
  end
end

convolucion = conv2(x,h)

imagen = imread('./lena.tiff');

imf = double(imagen);
imbw = (imf(:,:,1) + imf(:,:,2) + imf(:,:,3))./3;

convImagen = conv2(imbw, [5,-5 ; 0,0]);
imbwi = uint8(convImagen);
imshow(imbwi)

pause

convImagen2 = conv2(imbw, [5,0 ; -5,0]);
imbwi2 = uint8(convImagen2);
imshow(imbwi2)
pause

ge = sqrt(convImagen.^2 + convImagen2.^2);
gelimit = max(min(ge,255),0);
gef = uint8(gelimit);
imshow(gef)


pause


conv2Imagen = conv2(imbw, [1,0,-1; 2,0,-2; 1,0,-1]);
imbw2i = uint8(conv2Imagen);
imshow(imbw2i)

pause

conv2Imagen2 = conv2(imbw, [1,2,1; 0,0,0; -1,-2,-1]);
imbw2i2 = uint8(conv2Imagen2);
imshow(imbw2i2)
pause

ge2 = sqrt(conv2Imagen.^2 + conv2Imagen2.^2);
gelimit2 = max(min(ge2,255),0);
gef2 = uint8(gelimit2);
imshow(gef2)


pause