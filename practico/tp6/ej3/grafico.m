#!/usr/bin/env octave-cli

frec_muestreo = 39062.5
muestras_fft = 32

eje_x = 1:frec_muestreo/muestras_fft:frec_muestreo/2;

graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");

figure(3, "name", "Absoluto", "NumberTitle", "off", "position", tamanio);
plot_Abs = plot(eje_x, eje_x);

fd_uart=fopen('/dev/ttyACM0');


do
	datos = fread(fd_uart,35);
	if(datos(35)!=85)
		do
			syanc = fread(fd_uart,1);
		until(syanc==85)
	else
	for x = 1:35
		if datos(x)>127
			datos(x) = datos(x) - 255;
		end
	endfor

	set(plot_Abs, 'YData', abs(complex(datos(2:17),datos(19:34))));
	drawnow();
	end
until(1==2)
