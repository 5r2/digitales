#!/usr/bin/env octave-cli

%graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");

t=0:0.001:2

x=2*sin(20*pi*t) + sin(100*pi*t);;
subplot(2,1,1);
plot(1000*t,x);
grid;
xlabel("Time in milliseconds");
ylabel("Signal amplitude");

subplot(2,1,2);
y=fft(x);
%plot(1000*t,abs(y))
plot(1000*t,real(y));
subplot(2,2,2);
plot(1000*t,imag(y));
xlabel("Frequency");
ylabel("Signal amplitude");
pause;
