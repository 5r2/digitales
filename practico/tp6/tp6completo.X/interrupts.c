#include <xc.h>
#include <stdint.h>        /* Includes uint16_t definition   */
#include <stdbool.h>       /* Includes true/false definition */
#include <dsp.h>
#include <p33FJ128GP804.h>

void __attribute__((__interrupt__, no_auto_psv)) _T3Interrupt(void)
{
    LATBbits.LATB3 = ~LATBbits.LATB3;
    IFS0bits.T3IF = 0;        // Clear Timer3 Interrupt Flag
}

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void)
{
    extern uint16_t datosDAC_A[], datosDAC_B[];
    extern fractional temporal[];

    if(DMACS1bits.PPST0 == 1)
        VectorCopy(32, (fractional *)datosDAC_A, temporal);
    else
        VectorCopy(32, (fractional *)datosDAC_B, temporal);
  
    IFS0bits.DMA0IF = 0;
}

void __attribute__((interrupt, no_auto_psv)) _DMA1Interrupt(void)
{
    extern uint8_t estado;
    extern fractional temporal[], temporal_LP[], temporal_HP[], temporal_BP[];
    
    extern FIRStruct filtro3kHz;
    extern uint16_t datosADC_A[], datosADC_B[];
    extern uint16_t datosDAC_A[], datosDAC_B[];
    
    extern IIRTransposedStruct iirLP, iirHP, iirBP;

    extern fractional ganancia_LP, ganancia_HP, ganancia_BP;
    extern uint16_t actualizacionADC, actualizacionFFT;
    extern fractcomplex fourier_giro[] __attribute__((space(xmemory), aligned(64)));
    extern fractcomplex fourier_origen[] __attribute__((space(ymemory), aligned(128)));
    extern fractcomplex fourier_destino[] __attribute__((space(ymemory), aligned(128)));
    extern fractional ventana_hanning[];
    fractional buffer_datos[32];

    uint8_t i;

    
    switch(estado)
    {
        case 0: //ej1    
            if(DMACS1bits.PPST1 == 1)
                FIR(32, temporal, (fractional *)datosADC_A, &filtro3kHz);
            else
                FIR(32, temporal, (fractional *)datosADC_B, &filtro3kHz);
            break;       
        case 1: //ej2
            if(DMACS1bits.PPST1 == 1)
            {
                IIRTransposed(32, temporal_LP, (fractional*)datosADC_A, &iirLP);
                IIRTransposed(32, temporal_HP, (fractional*)datosADC_A, &iirHP);
                IIRTransposed(32, temporal_BP, (fractional*)datosADC_A, &iirBP);
            }
            else
            {
                IIRTransposed(32, temporal_LP, (fractional*)datosADC_B, &iirLP);
                IIRTransposed(32, temporal_HP, (fractional*)datosADC_B, &iirHP);
                IIRTransposed(32, temporal_BP, (fractional*)datosADC_B, &iirBP);
            }
            VectorScale(32, temporal_LP, temporal_LP, ganancia_LP);
            VectorScale(32, temporal_HP, temporal_HP, ganancia_HP);
            VectorScale(32, temporal_BP, temporal_BP, ganancia_BP);
            VectorAdd(32, temporal, temporal_LP, temporal_HP);
            VectorAdd(32, temporal, temporal, temporal_BP);

            break;
        case 2: //ej3
             
            // Asi se implementa como lo pide el ejercicio pero no tiene sentido practico 
            if (actualizacionADC >= 61){ // 39062/32/20
                for (i=0;i<32;i++)
                {
                    if (DMACS1bits.PPST1==1)
                        buffer_datos[i] = datosADC_A[i];
                    else
                        buffer_datos[i] = datosADC_B[i];
                }
                    
                if( PORTBbits.RB7 == 1)
                    VectorWindow(32, buffer_datos, buffer_datos, ventana_hanning);                    
                                    
                for(i=0;i<32;i++){
                    fourier_origen[i].imag = 0;
                    fourier_origen[i].real = buffer_datos[i];
                }
                
                FFTComplex(5, fourier_destino, fourier_origen, fourier_giro, 0xFF00);
                while(!U1STAbits.TRMT);
                U1TXREG = 0xAA;
                while(!U1STAbits.TRMT);
                for (i=0; i<16; i++){
                    U1TXREG = fourier_destino[i].real >> 8;
                    while(!U1STAbits.TRMT);                
                }
                U1TXREG = 0x80;
                while(!U1STAbits.TRMT);                
                for (i=0; i<16; i++){
                    U1TXREG = fourier_destino[i].imag >> 8;
                    while(!U1STAbits.TRMT);                
                }    
                U1TXREG = 0x55;
                while(!U1STAbits.TRMT);                
                
                actualizacionADC = 0;

            }
            actualizacionADC++;
            
            /*
            // Ahora se implementa como pareciera que tiene que ser
                if (actualizacionADC >= 0){ // 39062/32/20
                    actualizacionADC = 0;
                    
                    if (DMACS1bits.PPST1==1)
                        buffer_datos[actualizacionFFT] = datosADC_A[actualizacionFFT];
                    else
                        buffer_datos[actualizacionFFT] = datosADC_B[actualizacionFFT];
                    actualizacionFFT++;
                }
                
                if(actualizacionFFT >= 32){
                    actualizacionFFT = 0;
                    
                    if( PORTBbits.RB7 == 1)
                        VectorWindow(32, buffer_datos, buffer_datos, ventana_hanning);                    
                    
                    for(i=0;i<32;i++){
                        fourier_origen[i].imag = 0;
                        fourier_origen[i].real = buffer_datos[i];
                    }
                    
                    FFTComplex(5, fourier_destino, fourier_origen, fourier_giro, 0xFF00);
                    
                    while(!U1STAbits.TRMT);
                    U1TXREG = 0xAA;
                    while(!U1STAbits.TRMT);
                    for (i=0; i<16; i++){
                        U1TXREG = fourier_destino[i].real >> 8;
                    while(!U1STAbits.TRMT);                
                    }
                    U1TXREG = 0x80;
                    for (i=16; i<32; i++){
                        U1TXREG = fourier_destino[i].imag >> 8;
                        while(!U1STAbits.TRMT);                
                    }    
                    U1TXREG = 0x55;
                    while(!U1STAbits.TRMT);                
                }
                actualizacionADC++;
             */
            break;
        default:
            break;
    }
    IFS0bits.DMA1IF = 0;
}

void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void)
{
    extern fractional ganancia_LP, ganancia_HP, ganancia_BP;
    extern uint8_t estado;

    LATBbits.LATB2 = 1;
    uint16_t i = 0;
    switch(estado)
    {
        case 0: //ej1    
            break;       
        case 1: //ej2
            switch (U1RXREG)
            {
                case 'q':
                    if(ganancia_LP+0xCCD<0x7FFF && ganancia_LP+0xCCD>0x0000)
                        ganancia_LP += 0x0CCD;
                    break;
                case 'w':
                    if(ganancia_HP+0xCCD<0x7FFF && ganancia_HP+0xCCD>0x0000)
                        ganancia_HP += 0x0CCD;            
                    break;
                case 'e':
                    if(ganancia_BP+0xCCD<0x7FFF && ganancia_BP+0xCCD>0x0000)
                        ganancia_BP += 0x0CCD;            
                    break;
                case 'a':
                    if(ganancia_LP-0xCCD<0x7FFF && ganancia_LP-0xCCD>0x0000)
                        ganancia_LP -= 0x0CCD;            
                    break;
                case 's':
                    if(ganancia_HP-0xCCD<0x7FFF && ganancia_HP-0xCCD>0x0000)
                        ganancia_HP -= 0x0CCD;            
                    break;
                case 'd':
                    if(ganancia_BP-0xCCD<0x7FFF && ganancia_BP-0xCCD>0x0000)
                        ganancia_BP -= 0x0CCD;            
                    break;
            }
            for(i=0; i<4095; i++);
            break;
        case 2: //ej3
            break;
        default:
            break;
    }

    LATBbits.LATB2 = 0;
    IFS0bits.U1RXIF = 0;
}