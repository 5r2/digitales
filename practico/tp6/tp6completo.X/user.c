#include <xc.h>
#include <stdint.h>          /* For uint16_t definition                       */
#include <stdbool.h>
#include <dsp.h>         /* For true/false definition                     */
#include "user.h"            /* variables/params used by user.c               */
#include "system.h"            /* variables/params used by user.c               */

void inicializacion(void)
{
    extern FIRStruct filtro3kHz;
    extern fractional coeficientes[], retardos[];
    extern IIRTransposedStruct iirLP, iirHP, iirBP;
    extern fractional retardo_LP_a[], retardo_LP_b[], retardo_HP_a[], retardo_HP_b[], retardo_BP_a[], retardo_BP_b[];
    extern fractional coeffsLP[], coeffsHP[], coeffsBP[];
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB7 = 1;
    TRISBbits.TRISB10 = 1;
    
    
    filtro3kHz.numCoeffs = 401;
    filtro3kHz.coeffsBase = coeficientes;
    filtro3kHz.coeffsEnd = &((uint8_t *)coeficientes)[801];
    filtro3kHz.coeffsPage = 0xFF00;
    filtro3kHz.delayBase = retardos;
    filtro3kHz.delayEnd = &((uint8_t *)retardos)[801];
    filtro3kHz.delay = filtro3kHz.delayBase;
    
    FIRDelayInit(&filtro3kHz);
    
    iirLP.numSectionsLess1 = 3;
    iirLP.coeffsBase = coeffsLP;
    iirLP.coeffsPage = 0xFF00;
    iirLP.delayBase1 = &retardo_LP_a[0];
    iirLP.delayBase2 = &retardo_LP_b[0];
    iirLP.finalShift = 0;
    IIRTransposedInit(&iirLP);
       
    iirHP.numSectionsLess1 = 3;
    iirHP.coeffsBase = coeffsHP;
    iirHP.coeffsPage = 0xFF00;
    iirHP.delayBase1 = &retardo_HP_a[0];
    iirHP.delayBase2 = &retardo_HP_b[0];
    iirHP.finalShift = 0;
    IIRTransposedInit(&iirHP);  

    iirBP.numSectionsLess1 = 7;
    iirBP.coeffsBase = &coeffsBP[0];
    iirBP.coeffsPage = 0xFF00;
    iirBP.delayBase1 = &retardo_BP_a[0];
    iirBP.delayBase2 = &retardo_BP_b[0];
    iirBP.finalShift = 0;
    IIRTransposedInit(&iirBP);      
}

void ledReloj(void)
{
    configTimer3();
    TMR3 = 0x00;              // Clear 32-bit Timer (msw)
    TMR2 = 0x00;              // Clear 32-bit Timer (lsw)
    PR3 = 0x40;             // Load 32-bit period value (msw)
    PR2 = 0x00;             // Load 32-bit period value (lsw)
}

void filtroNotch3kHz(void)
{
    configOscilador();
    extern uint16_t datosADC_A[]__attribute__((space(dma)));
    extern uint16_t datosADC_B[]__attribute__((space(dma)));
    extern uint16_t datosDAC_A[]__attribute__((space(dma)));
    extern uint16_t datosDAC_B[]__attribute__((space(dma)));


    configDMA0();
    DMA0PAD = (volatile unsigned int) &DAC1RDAT;  /* Point DMA to DAC1RDAT */
    DMA0CNT = 31;                                /* 32 DMA Request */
    DMA0REQ = 78;                                /* Select DAC1RDAT as DMA Request Source */
    DMA0STA = __builtin_dmaoffset(datosDAC_A);
    DMA0STB = __builtin_dmaoffset(datosDAC_B);   

    configDMA1();
    DMA1PAD = (volatile unsigned int) &ADC1BUF0;  /* Point DMA to DAC1LDAT */
    DMA1CNT = 31;                                /* 32 DMA Request */
    DMA1REQ = 13;                                /* Select DAC1LDAT as DMA Request Source */
    DMA1STA = __builtin_dmaoffset(datosADC_A);
    DMA1STB = __builtin_dmaoffset(datosADC_B);
    
    configDAC(15,0);
    DAC1DFLT = 0x0000;

    configADC();
    configTimer5ADC();
}

#define FP 40000000
#define BAUDRATE 9600
#define BRGVAL ((FP/BAUDRATE)/16) - 1

void ecualizadorIIR(void)
{
    extern uint16_t datosADC_A[]__attribute__((space(dma)));
    extern uint16_t datosADC_B[]__attribute__((space(dma)));
    extern uint16_t datosDAC_A[]__attribute__((space(dma)));
    extern uint16_t datosDAC_B[]__attribute__((space(dma)));

    configOscilador();
    configUART();
    U1BRG = BRGVAL; 
    configDMA0();
    DMA0PAD = (volatile unsigned int) &DAC1RDAT;  /* Point DMA to DAC1RDAT */
    DMA0CNT = 31;                                /* 32 DMA Request */
    DMA0REQ = 78;                                /* Select DAC1RDAT as DMA Request Source */
    DMA0STA = __builtin_dmaoffset(datosDAC_A);
    DMA0STB = __builtin_dmaoffset(datosDAC_B);   

    configDMA1();
    DMA1PAD = (volatile unsigned int) &ADC1BUF0;  /* Point DMA to DAC1LDAT */
    DMA1CNT = 31;                                /* 32 DMA Request */
    DMA1REQ = 13;                                /* Select DAC1LDAT as DMA Request Source */
    DMA1STA = __builtin_dmaoffset(datosADC_A);
    DMA1STB = __builtin_dmaoffset(datosADC_B);
    
    configDAC(15,0);
    DAC1DFLT = 0x0000;

    configADC();
    configTimer5ADC();
}
void analizadorFourier(void)
{
    extern uint16_t datosADC_A[]__attribute__((space(dma)));
    extern uint16_t datosADC_B[]__attribute__((space(dma)));
    extern fractcomplex fourier_giro[];
    extern fractional ventana_hanning[];

    configOscilador();
    configUART();
    U1BRG = BRGVAL; 

    configDMA1();
    DMA1PAD = (volatile unsigned int) &ADC1BUF0;  /* Point DMA to DAC1LDAT */
    DMA1CNT = 31;                                /* 32 DMA Request */
    DMA1REQ = 13;                                /* Select DAC1LDAT as DMA Request Source */
    DMA1STA = __builtin_dmaoffset(datosADC_A);
    DMA1STB = __builtin_dmaoffset(datosADC_B);

    configADC();
    configTimer5ADC();
    
    TwidFactorInit(5,fourier_giro,0);
    HanningInit(32, ventana_hanning);
}