#!/usr/bin/env octave-cli

bitsCuantizacion = 16
frecMuestreoHz = 39062.5
frecCentralHz = 3000
anchoFiltroHz = 400
tipoVentana = "Hamming"
graficarBonito = 1

pkg load control;
pkg load geometry;
pkg load signal;

graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");
figure(1,"position", tamanio);

function bincoeffs = cuantizarCoeficientes2(bits, coeficientes)
  maxbpos = (2^(bits-1))-1;
  maxbneg = (2^(bits-1));
  hnorm = coeficientes./max(coeficientes);
  ptrneg = find(hnorm<0);
  hQ = hnorm .* maxbpos;
  hQ(ptrneg) = hnorm(ptrneg) .* maxbneg;
  hQ = round(hQ);
  bincoeffs = hQ;
endfunction

function [out] = num2strhex(num,bits)
	if(num<0)
		num = num + (2**bits-1)+1;
	end
	formato = strcat(strcat("%0",num2str(round(log(2**bits)/log(bits)))), "x");
	out = num2str(num,formato);
end

function coeffs = generarFiltro(fpass, fstop, ventana, anchoTransicion)
  switch(ventana)
    case "Rectangular"
      M = 4/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = rectwin(M);
    case "Hanning"
      M = 8/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = hann(M);
    case "Hamming"
      M = 8/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = hamming(M);
    case "Blackman"
      M = 12/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = blackman(M);
    otherwise
      M = 4/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = rectwin(M);
  endswitch
  
  ft = (fpass+fstop)/2;
  
  for n = 1:M
    nciclo = n - (M-1)/2;
    hsinc(n) = ft*sinc(ft*(nciclo));
    hsinc(n) = hsinc(n) * mVentana(n);
  endfor

  coeffs = hsinc;
endfunction

function bincoeffs = cuantizarCoeficientes(bits, coeficientes)
  maxbpos = (2^(bits-1))-1;
  maxbneg = (2^(bits-1));
  hnorm = coeficientes./max(coeficientes);
  ptrneg = find(hnorm<0);
  hQ = hnorm .* maxbpos;
  hQ(ptrneg) = hnorm(ptrneg) .* maxbneg;
  hQ = round(hQ);
  bincoeffs = hQ;
endfunction

function respuestaDB(coeficientes, hz, nombre)
  muestras = 10000;
  H = fft(coeficientes, muestras);
  dBHpos = 20*log10(abs(H(1:muestras/2)));
  rango = [0:(1/(muestras/2-1))*hz:1*hz];
  grafico = plot(rango, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction

function invertirFiltro(coeficientes, hz, nombre)
  muestras = 10000;
  ceros = zeros(1,size(coeficientes)(2)/2-1);
  retardo = [ceros,1];
  H = fft(retardo, muestras) - fft(coeficientes, muestras);
  coeffs = H
  dBHpos = 20*log10(abs(H(1:muestras/2)));
  rango = [0:(1/(muestras/2-1))*hz:1*hz];
  grafico = plot(rango, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction

function coeffs = eliminaBanda(fc1, fc2, B, ventana)
  pasaBajo = generarFiltro(fpass=fc1-B, fstop = fc1+B, ventana = ventana, anchoTransicion =B);
  pasaAlto = generarFiltro(fpass=1-fc2+B, fstop = 1-fc2-B, ventana = ventana, anchoTransicion =B);
  pasaAlto(1:2:end) *= -1;
  coeffs = pasaAlto .+ pasaBajo;
endfunction



corteInferiorHz = frecCentralHz - (anchoFiltroHz*0.5);
corteSuperiorHz = frecCentralHz + (anchoFiltroHz*0.5);
anchoFiltro = anchoFiltroHz*0.5/frecMuestreoHz;
fNyquist = frecMuestreoHz/2;

coefsReal = eliminaBanda(fc1=corteInferiorHz/fNyquist, fc2=corteSuperiorHz/fNyquist, B=0.02, ventana = tipoVentana);
coefsBin = cuantizarCoeficientes(bitsCuantizacion,coefsReal);

disp("Vector de coeficientes en hexa:")
printf("{")
for ii = coefsBin
	printf("0x");
	printf(num2strhex(ii,bitsCuantizacion));
	printf(",");
end
disp("\b};");

disp(strcat("Cantidad de taps: ",num2str(size(coefsBin)(2))));

if(graficarBonito == 1)
 	respuestaDB(coefsReal, fNyquist, "Respuesta real");
 	pause
 	respuestaDB(coefsBin, fNyquist, "Respuesta cuantizada");
 	pause
else
	setenv GNUTERM dumb;
	x = num2str(str2num(nthargout(2,"system","tput lines")));
	y = num2str(str2num(nthargout(2,"system","tput cols")));
	putenv('LINES', x);
	putenv('COLUMNS', y);
	respuestaDB(coefsBin, fNyquist, "Respuesta cuantizada");
	drawnow
	respuestaDB(coefsReal, fNyquist, "Respuesta real");
	drawnow
end
