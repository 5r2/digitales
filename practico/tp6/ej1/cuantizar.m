#!/usr/bin/env octave-cli

coefs = [-3.1, -1.9, -1, -0.5, -0.25, -0.1, 0, 0.1, 0.25, 0.5, 1, 1.9, 2];
cBits = 8;

function bincoeffs = cuantizarCoeficientes2(bits, coeficientes)
  maxbpos = (2^(bits-1))-1;
  maxbneg = (2^(bits-1));
  hnorm = coeficientes./max(abs(coeficientes));
  ptrneg = find(hnorm<0);
  hQ = hnorm .* maxbpos;
  hQ(ptrneg) = hnorm(ptrneg) .* maxbneg;
  hQ = round(hQ);
  bincoeffs = hQ;
endfunction

function [out] = num2strdec(num)
	out = num2str(num);
end
function [out] = num2strbin(num,bits)
	if(num<0)
		num = num + (2**bits-1)+1;
	end
	formato = strcat(strcat("%0",num2str(bits)), "d");
	out = num2str(str2num(dec2bin(num)),formato);
end
function [out] = num2strhex(num,bits)
	if(num<0)
		num = num + (2**bits-1)+1;
	end
	formato = strcat(strcat("%0",num2str(round(log(2**bits)/log(bits)))), "x");
	out = num2str(num,formato);
end

function [res] = coefs2dec(coefs)
	res = arrayfun(@num2strdec,coefs,"UniformOutput",false);
end
function [res] = coefs2hex(coefs, bits)
	res = arrayfun(@num2strhex,coefs,bits,"UniformOutput",false);
end
function [res] = coefs2bin(coefs, bits)
	res = arrayfun(@num2strbin,coefs,bits,"UniformOutput",false);
end

cuantizados = cuantizarCoeficientes2(cBits, coefs);
coefsDec = coefs2dec(cuantizados);
coefsHex = coefs2hex(cuantizados, cBits);
coefsBin = coefs2bin(cuantizados, cBits);

disp("Coeficientes reales: ");
	disp(coefs); disp("")
disp("Cantidad de bits: ");
	disp(cBits); disp("")
disp("Coeficientes cuantizados en decimal: ");
	disp(coefsDec); disp("");
disp("Coeficientes cuantizados en hexadecimal: ");
	disp(coefsHex); disp("");
disp("Coeficientes cuantizados en binario: ");
	disp(coefsBin); disp("");
