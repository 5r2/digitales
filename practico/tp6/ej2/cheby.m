#!/usr/bin/env octave-cli

bitsCuantizacion = 16
frecMuestreoHz = 39062.5
frecLP = 500
frecHP = 3000
frecBP_l = 700
frecBP_h = 2000
ordenFiltro = 2
rippleFiltroDB = 0.1
graficarBonito = 1

pkg load control;
pkg load geometry;
pkg load signal;

graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");
figure(1,"position", tamanio);

function respuestaDBIIR(a, b, sample,nombre)
  imp = [1,zeros(1,sample-1),zeros(1,sample*3)];
  respuesta = filter(b,a,imp);
  muestras = 10000;
  freqPos = muestras/2;
  H = fft(respuesta, muestras);
  dBHpos = 20*log10(abs(H(1:freqPos)));
  rango = [0:(1/(freqPos-1)):1];
  grafico = plot(rango*sample/2, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction
function respuestaDBIIRx4(a, b, sample,nombre)
  imp = [1,zeros(1,sample-1),zeros(1,sample*3)];
  respuesta = filter(b,a,imp);
  for i = 1:3
    respuesta = filter(b,a,respuesta);
  endfor
  muestras = 10000;
  freqPos = muestras/2;
  H = fft(respuesta, muestras);
  dBHpos = 20*log10(abs(H(1:freqPos)));
  rango = [0:(1/(freqPos-1)):1];
  grafico = plot(rango*sample/2, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction

function respuestaDBIIRBP(a_l, b_l, a_h, b_h, sample,nombre)
  imp = [1,zeros(1,sample-1),zeros(1,sample*3)];
  respuesta = filter(b_l,a_l,imp);
  respuesta = filter(b_h,a_h,respuesta);
  muestras = 10000;
  freqPos = muestras/2;
  H = fft(respuesta, muestras);
  dBHpos = 20*log10(abs(H(1:freqPos)));
  rango = [0:(1/(freqPos-1)):1];
  grafico = plot(rango*sample/2, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction
function respuestaDBIIRBPx4(a_l, b_l, a_h, b_h, sample,nombre)
  imp = [1,zeros(1,sample-1),zeros(1,sample*3)];
  respuesta = filter(b_l,a_l,imp);
  respuesta = filter(b_h,a_h,respuesta);
  for i = 1:3
    respuesta = filter(b_l,a_l,respuesta);
  endfor
  for i = 1:3
    respuesta = filter(b_h,a_h,respuesta);
  endfor
  muestras = 10000;
  freqPos = muestras/2;
  H = fft(respuesta, muestras);
  dBHpos = 20*log10(abs(H(1:freqPos)));
  rango = [0:(1/(freqPos-1)):1];
  grafico = plot(rango*sample/2, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction

function [out] = num2strhex(num,bits)
	if(num<0)
		num = num + (2**bits-1)+1;
	end
	formato = strcat(strcat("%0",num2str(round(log(2**bits)/log(bits)))), "x");
	out = num2str(num,formato);
end

function bincoeffs = cuantizarCoeficientes(bits, coeficientes)
  maxbpos = (2^(bits-1))-1;
  maxbneg = (2^(bits-1));
  hnorm = coeficientes./max(coeficientes);
  ptrneg = find(hnorm<0);
  hQ = hnorm .* maxbpos;
  hQ(ptrneg) = hnorm(ptrneg) .* maxbneg;
  hQ = round(hQ);
  bincoeffs = hQ;
endfunction

[lowpass_b, lowpass_a] = cheby1(ordenFiltro, rippleFiltroDB, frecLP*2/frecMuestreoHz, "low");
[highpass_b, highpass_a] = cheby1(ordenFiltro, rippleFiltroDB, frecHP*2/frecMuestreoHz, "high");
[bandpass_l_b, bandpass_l_a] = cheby1(ordenFiltro, rippleFiltroDB, frecBP_l*2/frecMuestreoHz, "low");
[bandpass_h_b, bandpass_h_a] = cheby1(ordenFiltro, rippleFiltroDB, frecBP_h*2/frecMuestreoHz, "high");
lowpass_a_bin = cuantizarCoeficientes(bitsCuantizacion, lowpass_a);
lowpass_b_bin = cuantizarCoeficientes(bitsCuantizacion, lowpass_b);
highpass_a_bin = cuantizarCoeficientes(bitsCuantizacion, highpass_a);
highpass_b_bin = cuantizarCoeficientes(bitsCuantizacion, highpass_b);
bandpass_l_a_bin = cuantizarCoeficientes(bitsCuantizacion, bandpass_l_a);
bandpass_l_b_bin = cuantizarCoeficientes(bitsCuantizacion, bandpass_l_b);
bandpass_h_a_bin = cuantizarCoeficientes(bitsCuantizacion, bandpass_h_a);
bandpass_h_b_bin = cuantizarCoeficientes(bitsCuantizacion, bandpass_h_b);

disp("Vector de coeficientes en hexa:")
function vector2bitshex(vector_a, vector_b, bits)
  printf("{")
  for repeticion = 1:4
    printf("0x"); printf(num2strhex(vector_b(1),bits));printf(",");
    for ii = 2:size(vector_b)(2)
      printf("0x");
      printf(num2strhex(vector_b(ii),bits));
      printf(",");
      printf("0x");
      printf(num2strhex(vector_a(ii),bits));
      printf(",");
    end
  endfor
  disp("\b};");
endfunction
disp("Bajos:")
vector2bitshex(lowpass_a_bin, lowpass_b_bin, bitsCuantizacion);
disp("Altos:")
vector2bitshex(highpass_a_bin, highpass_b_bin, bitsCuantizacion);
disp("Banda Bajos:")
vector2bitshex(bandpass_l_a_bin, bandpass_l_b_bin, bitsCuantizacion);
disp("Banda Altos:")
vector2bitshex(bandpass_h_a_bin, bandpass_h_b_bin, bitsCuantizacion);

if(graficarBonito == 0)
  setenv GNUTERM dumb;
  x = num2str(str2num(nthargout(2,"system","tput lines")));
  y = num2str(str2num(nthargout(2,"system","tput cols")));
  putenv('LINES', x);
  putenv('COLUMNS', y);
end

respuestaDBIIR(lowpass_a, lowpass_b, frecMuestreoHz, "Pasa Bajo");
hold on;
respuestaDBIIR(lowpass_a_bin, lowpass_b_bin, frecMuestreoHz, "Pasa Bajo");
respuestaDBIIRx4(lowpass_a, lowpass_b, frecMuestreoHz, "Pasa Bajo x4");
pause;
hold off;
respuestaDBIIR(highpass_a, highpass_b, frecMuestreoHz, "Pasa Alto");
hold on;
respuestaDBIIRx4(highpass_a, highpass_b, frecMuestreoHz, "Pasa Alto x4");
pause;
hold off;
respuestaDBIIR(bandpass_l_a, bandpass_l_b, frecMuestreoHz, "Pasa Banda Bajo");
hold on;
respuestaDBIIR(bandpass_h_a, bandpass_h_b, frecMuestreoHz, "Pasa Banda Alto");
pause;
hold off;
respuestaDBIIRBP(bandpass_l_a, bandpass_l_b, bandpass_h_a, bandpass_h_b, frecMuestreoHz, "Pasa Banda");
hold on;
respuestaDBIIRBPx4(bandpass_l_a, bandpass_l_b, bandpass_h_a, bandpass_h_b, frecMuestreoHz, "Pasa Banda x4");
pause;
hold off;
