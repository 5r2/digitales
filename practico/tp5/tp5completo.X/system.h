/* TODO Define system operating frequency */

/* Microcontroller MIPs (FCY) */
#define SYS_FREQ        7370000L
#define FCY             SYS_FREQ/2


void configOscilador(void); /* Handles clock switching/osc initialization */
void configDAC(uint16_t, uint16_t);
void configTimer3(void);
void configTimer5(void);
void configTimer5ADC(void);
void configDMA0(void);
void configDMA1(void);
void configDMA2(void);
void configADC();
void configUART(void);