void inicializacion(void);         /* I/O and Peripheral Initialization */
void ledBoton(void);
void ledToggle(void);
void ledTimer(void);
void ledReloj(void);
void ledTimer40MIPS(void);
void dacAlternar(void);
void dacSenoidal(void);
void adcCopia(void);
void uart(void);