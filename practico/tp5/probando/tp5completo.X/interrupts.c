#include <xc.h>

#include <stdint.h>        /* Includes uint16_t definition   */
#include <stdbool.h>       /* Includes true/false definition */

/* Example code for Timer3 ISR */
void __attribute__((__interrupt__, no_auto_psv)) _T3Interrupt(void)
{
    LATBbits.LATB3 = ~LATBbits.LATB3;
    IFS0bits.T3IF = 0;        // Clear Timer3 Interrupt Flag
}

void __attribute__((__interrupt__, no_auto_psv)) _T5Interrupt(void)
{
    LATBbits.LATB2 = ~LATBbits.LATB2;
    IFS1bits.T5IF = 0;        // Clear Timer3 Interrupt Flag
}

void __attribute__((interrupt, no_auto_psv)) _DAC1RInterrupt(void)
{
    extern uint16_t salidaDacR;
    if (salidaDacR==0x8000)
        salidaDacR=0x7FFF;
    else
        salidaDacR=0x8000;
    DAC1RDAT = salidaDacR;                   /* User Code to Write to FIFO Goes Here */
    IFS4bits.DAC1RIF = 0;                    /* Clear Right Channel Interrupt Flag */
}

void __attribute__((interrupt, no_auto_psv)) _DAC1LInterrupt(void)
{
    extern uint16_t salidaDacL;
    if (salidaDacL==0x8000)
        salidaDacL=0x7FFF;
    else
        salidaDacL=0x8000;    
    DAC1LDAT = salidaDacL;                   /* User Code to Write to FIFO Goes Here */
    IFS4bits.DAC1LIF = 0;                    /* Clear Left Channel Interrupt Flag */
}

void __attribute__((interrupt, no_auto_psv)) _DMA0Interrupt(void)
{
    IFS0bits.DMA0IF = 0;                         /* Clear DMA Channel 0 Interrupt Flag */
}

void __attribute__((interrupt, no_auto_psv)) _DMA1Interrupt(void)
{
    IFS0bits.DMA1IF = 0;                         /* Clear DMA Channel 1 Interrupt Flag */
}

void __attribute__((interrupt, no_auto_psv)) _DMA2Interrupt(void)
{
    IFS1bits.DMA2IF = 0;                         /* Clear DMA Channel 1 Interrupt Flag */
}

void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void)
{
    LATBbits.LATB2 = 1;
    uint16_t i = 0;
    //U1TXREG = 'R'; U1TXREG = 'e'; U1TXREG = 'c'; U1TXREG = 'i'; U1TXREG = 'b'; U1TXREG = 'i'; U1TXREG = 'd'; U1TXREG = 'o'; U1TXREG = ':'; U1TXREG = ' '; 
    U1TXREG = 'R'; U1TXREG = ':';
    U1TXREG = U1RXREG;
    U1TXREG = '\n';U1TXREG = '\r';
    for(i=0; i<4095; i++);
    LATBbits.LATB2 = 0;
    IFS0bits.U1RXIF = 0;
}