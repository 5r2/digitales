#include <xc.h>

#include <stdint.h>          /* For uint16_t definition                       */
#include <stdbool.h>         /* For true/false definition                     */
#include "user.h"            /* variables/params used by user.c               */
#include "system.h"            /* variables/params used by user.c               */

void inicializacion(void)
{
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB7 = 1;
    TRISBbits.TRISB10 = 1;
}

void ledBoton(void){
    LATBbits.LATB2 = ~PORTBbits.RB7;
}

void ledToggle(void)
{
    uint32_t i;
    for(i=0; i<155000; i++);
    LATBbits.LATB2 = ~LATBbits.LATB2;
}

void ledTimer(void)
{
    configTimer5();
    TMR5 = 0x00;              // Clear 32-bit Timer (msw)
    TMR4 = 0x00;              // Clear 32-bit Timer (lsw)
    PR5 = 0x40;             // Load 32-bit period value (msw)
    PR4 = 0x00;             // Load 32-bit period value (lsw)
}

void ledReloj(void)
{
    configTimer3();
    TMR3 = 0x00;              // Clear 32-bit Timer (msw)
    TMR2 = 0x00;              // Clear 32-bit Timer (lsw)
    PR3 = 0x40;             // Load 32-bit period value (msw)
    PR2 = 0x00;             // Load 32-bit period value (lsw)
}

void ledTimer40MIPS(void)
{
    configOscilador();
    configTimer5();
    TMR5 = 0x00;              // Clear 32-bit Timer (msw)
    TMR4 = 0x00;              // Clear 32-bit Timer (lsw)
    PR5 = 0x240;             // Load 32-bit period value (msw)
    PR4 = 0x00;             // Load 32-bit period value (lsw)
}


void dacAlternar(void)
{
    configOscilador();
    extern uint16_t salidaDacR;
    extern uint16_t salidaDacL;
    salidaDacR = 0x8000; 
    salidaDacL = 0x8000;
    configDAC(7,1);
    DAC1DFLT = 0x8000;                       /* Default value set to Midpoint when FORM = 0 */
}
    
    
void dacSenoidal(void)
{
    configOscilador();
    extern int16_t senoidal[]__attribute__((space(dma)));
    
    configDMA0();
    configDMA1();
    
    DMA0PAD = (volatile unsigned int) &DAC1RDAT;  /* Point DMA to DAC1RDAT */
    DMA0CNT = 31;                                /* 32 DMA Request */
    DMA0REQ = 78;                                /* Select DAC1RDAT as DMA Request Source */
    DMA0STA = __builtin_dmaoffset(senoidal);
    DMA0STB = __builtin_dmaoffset(senoidal);

    DMA1PAD = (volatile unsigned int) &DAC1LDAT;  /* Point DMA to DAC1LDAT */
    DMA1CNT = 31;                                /* 32 DMA Request */
    DMA1REQ = 79;                                /* Select DAC1LDAT as DMA Request Source */
    DMA1STA = __builtin_dmaoffset(senoidal);
    DMA1STB = __builtin_dmaoffset(senoidal);
    
    configDAC(15,0);
    DAC1DFLT = 0x0000;                       /* Default value set to Midpoint when FORM = 0 */
}

void adcCopia(void)
{
    configOscilador();
    extern int16_t datosADC[]__attribute__((space(dma)));
    extern int16_t senoidal[]__attribute__((space(dma)));
 

    configDMA0();
    DMA0PAD = (volatile unsigned int) &DAC1RDAT;  /* Point DMA to DAC1RDAT */
    DMA0CNT = 31;                                /* 32 DMA Request */
    DMA0REQ = 78;                                /* Select DAC1RDAT as DMA Request Source */
    DMA0STA = __builtin_dmaoffset(senoidal);
    DMA0STB = __builtin_dmaoffset(senoidal);   
    
    configDMA1();
    DMA1PAD = (volatile unsigned int) &DAC1LDAT;  /* Point DMA to DAC1LDAT */
    DMA1CNT = 31;                                /* 32 DMA Request */
    DMA1REQ = 79;                                /* Select DAC1LDAT as DMA Request Source */
    DMA1STA = __builtin_dmaoffset(datosADC);
    DMA1STB = __builtin_dmaoffset(datosADC);

    configDMA2();
    DMA2PAD = (volatile unsigned int) &ADC1BUF0;  /* Point DMA to DAC1RDAT */
    DMA2CNT = 31;                                /* 32 DMA Request */
    DMA2REQ = 13;                                /* Select DAC1RDAT as DMA Request Source */
    DMA2STA = __builtin_dmaoffset(datosADC);
    DMA2STB = __builtin_dmaoffset(datosADC);   
    
    configDAC(15,0);
    
    DAC1DFLT = 0x0000;                       /* Default value set to Midpoint when FORM = 0 */
    DMA1STA = __builtin_dmaoffset(datosADC);
    DMA1STB = __builtin_dmaoffset(datosADC);

    
    configADC();
    configTimer5ADC();
}

#define FP 40000000
#define BAUDRATE 9600
#define BRGVAL ((FP/BAUDRATE)/16) - 1

void uart(void){
    configOscilador();
    configUART();
    U1BRG = BRGVAL;                           // Baud Rate setting for 9600
}