#include <xc.h>

#include <stdint.h>          /* For uint16_t definition                       */
#include <stdbool.h>         /* For true/false definition                     */
#include "system.h"          /* variables/params used by system.c             */

void configOscilador(void)
{
    // Configure PLL prescaler, PLL postscaler, PLL divisor
    PLLFBD=30;             // M = 32
    CLKDIVbits.PLLPOST=0;  // N2 = 2
    CLKDIVbits.PLLPRE=0;   // N1 = 2
        
    // Initiate Clock Switch to Primary Oscillator with PLL (NOSC = 0b011)
    __builtin_write_OSCCONH(0x03);
    __builtin_write_OSCCONL(OSCCON | 0x01);
        
    // Espera que cambie el reloj
    while(OSCCONbits.COSC != 0b011){};
    
    // Espera que se enganche el PLL
    while(OSCCONbits.LOCK!=1){};
    
    // Setea divisor del clk para que divida por 1.
    ACLKCONbits.APSTSCLR = 0b111;
    ACLKCONbits.SELACLK = 0b000;
}



void configDAC(uint16_t clkdiv, uint16_t interrupts)
{ 
    DAC1STATbits.ROEN = 1;                   /* Right Channel DAC Output Enabled */ 
    DAC1STATbits.LOEN = 1;                   /* Left Channel DAC Output Enabled */
    
    DAC1STATbits.RITYPE = 1;                 /* Right Channel Interrupt if FIFO is not Full */ 
    DAC1STATbits.LITYPE = 1;                 /* Left Channel Interrupt if FIFO is not Full */ 
    
    DAC1CONbits.AMPON = 0;                   /* Amplifier Disabled During Sleep and Idle Modes */
    DAC1CONbits.DACFDIV = clkdiv;                 /* Divide Clock by 1 (Assumes Clock is 25.6MHz) */
    DAC1CONbits.FORM = 1;                    /* Data Format is Unsigned */
        
    IFS4bits.DAC1RIF = 0;                    /* Clear Right Channel Interrupt Flag */
    IFS4bits.DAC1LIF = 0;                    /* Clear Left Channel Interrupt Flag */
    
    IEC4bits.DAC1RIE = interrupts;                    /* Right Channel Interrupt Enabled */
    IEC4bits.DAC1LIE = interrupts;                    /* Left Channel Interrupt Enabled */
    
    DAC1CONbits.DACEN = 1;                   /* DAC1 Module Enabled */
    
        /* DAC1 Code */
}

void configTimer3(void)
{
    T3CONbits.TON = 0;        // Stop any 16-bit Timer3 operation
    T2CONbits.TON=  0;        // Stop any 16/32-bit Timer2 operation
    T2CONbits.T32 = 1;        // Enable 32-bit Timer mode
    T2CONbits.TCS = 0;        // Select internal instruction cycle clock 
    T2CONbits.TGATE = 0;      // Disable Gated Timer mode
    T2CONbits.TCKPS = 0b00;   // Select 1:1 Prescaler
    
    IPC2bits.T3IP = 0x01;     // Set Timer3 Interrupt Priority Level
    IFS0bits.T3IF = 0;        // Clear Timer3 Interrupt Flag
    IEC0bits.T3IE = 1;    // Enable Timer3 interrupt
    
    T2CONbits.TON = 1;        // Start 32-bit Timer
}

void configTimer5(void)
{
    T5CONbits.TON = 0;        // Stop any 16-bit Timer3 operation
    T4CONbits.TON=  0;        // Stop any 16/32-bit Timer2 operation
    T4CONbits.T32 = 1;        // Enable 32-bit Timer mode
    T4CONbits.TCS = 0;        // Select internal instruction cycle clock 
    T4CONbits.TGATE = 0;      // Disable Gated Timer mode
    T4CONbits.TCKPS = 0b00;   // Select 1:1 Prescaler
    
    IPC7bits.T5IP = 0x01;     // Set Timer3 Interrupt Priority Level
    IFS1bits.T5IF = 0;        // Clear Timer3 Interrupt Flag
    IEC1bits.T5IE = 1;    // Enable Timer3 interrupt
    
    T4CONbits.TON = 1;        // Start 32-bit Timer
}

void configTimer5ADC(void)
{
    T5CONbits.TON = 0;                          // Desabilita timer 5 (Frena cualquier actividad que este realizando)
    T5CONbits.TCS = 0;                          // Entrada de clk derivada del reloj de instrucciones interno Fcy=Fosc/2= 80Mhz/2=40MIPS(modo timer)
    T5CONbits.TGATE = 0;                        // Desabilita la entrada de reloj de modo timer por compuertas
    T5CONbits.TCKPS = 0b00;                     // Selecciona el preescaler en modo 1:1

    IPC7bits.T5IP = 0x01;                       // Seteo del nivel de prioridad de interrupcion del timer 5
    IFS1bits.T5IF = 0;                          // Limpia el Flag del timer 5
    IEC1bits.T5IE = 0;                          // Deshablitia la interrupcion del timer 5 (no hace falta ya que se usa para comandar el ADC)
    T5CONbits.TON = 1;                          // Inicia el Timer 5 de 16 bits
    
    TMR5 = 0x00;                                // Limpia los registros del timer 
    PR5 = 0x3FF;                                // Carga el valor de Periodo del timer de 16 bits = n�de cuentas - 1 = Fcy/fdeseada = 40MIPS/39062.5= 1024 cuentas = PR5+1
}

void configDMA0(void)
{
    DMA0CONbits.AMODE = 0;                       /* Register Indirect with Post Increment */
    DMA0CONbits.MODE = 2;                        /* Continuous Mode with Ping-Pong Enabled */
    DMA0CONbits.DIR = 1;                         /* Ram-to-Peripheral Data Transfer */
    IFS0bits.DMA0IF = 0;                         /* Clear DMA Interrupt Flag */
    IEC0bits.DMA0IE = 1;                         /* Set DMA Interrupt Enable Bit */
    DMA0CONbits.CHEN = 1;                        /* Enable DMA Channel 0 */ 
}

void configDMA1(void)
{
    DMA1CONbits.AMODE = 0;                       /* Register Indirect with Post Increment */
    DMA1CONbits.MODE = 2;                        /* Continuous Mode with Ping-Pong Enabled */
    DMA1CONbits.DIR = 1;                         /* Ram-to-Peripheral Data Transfer */
    IFS0bits.DMA1IF = 0;                         /* Clear DMA Interrupt Flag */
    IEC0bits.DMA1IE = 1;                         /* Set DMA Interrupt Enable Bit */
    DMA1CONbits.CHEN = 1;                        /* Enable DMA Channel 1 */
}

void configDMA2(void)
{
    DMA2CONbits.AMODE = 0;                       /* Register Indirect with Post Increment */
    DMA2CONbits.MODE = 2;                        /* Continuous Mode with Ping-Pong Enabled */
    DMA2CONbits.DIR = 0;                         /* Ram-to-Peripheral Data Transfer */
    IFS1bits.DMA2IF = 0;                         /* Clear DMA Interrupt Flag */
    IEC1bits.DMA2IE = 1;                         /* Set DMA Interrupt Enable Bit */
    DMA2CONbits.CHEN = 1;                        /* Enable DMA Channel 0 */
}

void configADC(void)
{
    AD1PCFGL = 0xFFFF;                          //Configurar todos los 32 bits como entradas digitales (*Referencia: pag 50, Manual ADC)
    AD1CON1bits.AD12B = 1;                      //Modo de operacion de ADC de 12 bits y canal simple
    AD1CON2bits.VCFG = 0b000;                   //Seleccion de Vdd y Vss como voltaje de referencia
    AD1CON3bits.ADRC = 0;                       //Seleccion del clock del adc dervidado del clock del sistema (Tcy=Tad)
    AD1CON3bits.ADCS = 0b00001001;              //Divisor del Clock por 10 (Tad= Tcy * (ADCS+1) = Tcy * 10)
    AD1PCFGLbits.PCFG0 = 0;                     //Configurar solo AN0 como entrada anal�gica      (*Referencia: pag 50, Manual ADC) 
    AD1CON2bits.CHPS = 0b00;                    //Conversion solo en el canal 0
    AD1CHS0bits.CH0SA = 0;                      //Seleccion de AN0 como entrada positiva para el muestreo del canal 0
    AD1CHS0bits.CH0NA = 0;                      //Seleccion de Vref- como entrada negativa para el muestreo del canal 0
    AD1CON1bits.ASAM = 1;                       //Habilita muestreo automatico
    AD1CON1bits.FORM = 0b11;                    //Tipo de datos fraccional con signo
    AD1CON2bits.SMPI = 0b0000;                  //Incrementa direccion del DMA por cada operacion muestra/conversion
    AD1CON1bits.SSRC = 0b100;                   //Timer 5 como temporizador del ADC1
    AD1CON3bits.SAMC = 0b00000101;              // Periodo de muestreo = 5 x Tad = SAMC*Tad
    AD1CON1bits.ADON = 1;                       //Encender el ADC1
}

void configUART(void)
{
    U1MODEbits.STSEL = 0;                    // 1-stop bit
    U1MODEbits.PDSEL = 0;                    // No Parity, 8-data bits
    U1MODEbits.ABAUD = 0;                    // Auto-Baud disabled
    U1MODEbits.BRGH = 0;                     // Standard-Speed mode
    U1STAbits.URXISEL = 0;                   // Interrupt after one RX character is received;
    IEC0bits.U1RXIE = 1;
    IFS0bits.U1RXIF = 0;     // Borrar flag de RX

    RPINR18bits.U1RXR = 5;   // Mapeo de entrada RX al pin RP5
    RPOR3bits.RP6R = 3;      // Mapeo de salida TX al pin RP6
    U1MODEbits.UARTEN = 1;   // Habilitar UART
    U1STAbits.UTXEN = 1;     // Habilitar Transmision de la UART
}