#include <xc.h>

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition                  */
#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp              */

uint16_t salidaDacR;
uint16_t salidaDacL;
uint16_t ijl=0;
uint16_t ijr=0;
int16_t senoidal[32]__attribute__((space(dma)))={0x0000,0x18F8,0x30FB,0x471C,0x5A81,0x6A6C,0x7640,0x7D89,0x7FFF,0x7D89,0x7640,0x6A6C,0x5A81,0x471C,0x30FB,0x18F8,0x10000,0xE707,0xCF04,0xB8E3,0xA57E,0x9593,0x89BF,0x8276,0x8001,0x8276,0x89BF,0x9593,0xA57E,0xB8E3,0xCF04,0xE707};
int16_t datosADC[32]__attribute__((space(dma)));
uint16_t estado __attribute__((persistent));


int16_t main(void)
{
    extern uint16_t estado;
    uint16_t primeraPasada = 1;
    inicializacion();
    ledReloj();
  
    if(estado > 7)
        estado = 0;
    
    while(1){
        if( PORTBbits.RB10 == 1)
        {
            if(estado >= 7)
            {
                estado = 0;
                asm ("RESET");
            }
            else
            {
                estado++;
                asm ("RESET");
            }
        }
        else
        {
            switch(estado)
            {
                case 0: //ej1p1
                    ledBoton();
                    break;
                case 1: //ej1p2
                    ledToggle();
                    break;
                case 2: //ej2p1
                    if(primeraPasada)
                    {
                        ledTimer();
                        primeraPasada = 0;
                    }
                    break;
                case 3: //ej2p2
                    if(primeraPasada)
                    {
                        ledTimer40MIPS();
                        primeraPasada = 0;
                    }
                    break;
                case 4: //ej3p1
                    if(primeraPasada)
                    {
                        dacAlternar();
                        primeraPasada = 0;
                    }
                    break;
                case 5: //ej3p2
                    if(primeraPasada)
                    {
                        dacSenoidal();
                        primeraPasada = 0;
                    }
                    break;
                case 6: //ej4p1
                    if(primeraPasada)
                    {
                        adcCopia();
                        primeraPasada = 0;
                    }
                    break;
                case 7: //ej5p1
                    if(primeraPasada)
                    {
                        uart();
                        primeraPasada = 0;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}