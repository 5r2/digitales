#!/usr/bin/env octave-cli

frecuenciaHz = 1;

muestras=2*frecuenciaHz;
puntos = 16*muestras;
for x = 1:puntos
       	seno(x) = sin(2*pi*(1/16)*(x-1)/(muestras));
endfor

medio = puntos/2;
senoentero(1:medio) = floor(seno(1:medio).*32767);
senoentero(medio+1:2*medio) = floor(seno(medio+1:2*medio).*32767)+65536;

printf("int senoidal[");
printf(num2str(puntos));
printf("]={");
for x = 1:(puntos)
	printf("0x");
	printf(dec2hex(senoentero(x),4));
	if(x<puntos)
		printf(",");
	endif
endfor
printf("};\n")

plot(seno);pause
