#!/usr/bin/env octave-cli

%equis = audioread("Jahzzar_44100.wav")(:,1).';
%ache = audioread("s1_r1_b_cd.wav")(:,1);
equis = randn(1,100000);
ache = randn(1,10000);
ene = numel(ache);
eme = numel(equis);

function q=fir(x,h,N,M)
  y = zeros(1, M+N-1);
  ene = zeros(1, N);
  x = [x,zeros(1,N-1)];
  
  for n=1:(M+N-1)
    ene = [x(n), ene];
    ene(end) = [];
    for i=1:N
      y(n) = y(n) + ene(i)*h(i);
    end
  end
  q=y;
end
  
function q=firMatricial(x,h,N,M)
  y = zeros(1, M+N-1);
  ene = zeros(1, N);
  x = [x,zeros(1,N-1)];
  
  for n=1:(M+N-1)
    ene = [x(n), ene];
    ene(end) = [];
    y(n) = h * ene.';
  end
  q=y;
end
  
%tic;
%a = fir(equis,ache,ene,eme);
%firTiempo = toc;
tic;
b = conv(equis,ache);
convTiempo = toc;
tic;
z = firMatricial(equis,ache,ene,eme);
matricialTiempo = toc;
%plot(a,'o-')
%hold all
%plot(z,'x-')
%hold all
%plot(b,'s-')

%disp("Tiempo del fir producto:"), disp(firTiempo)
disp("Tiempo del fir matricial:"), disp(matricialTiempo)
disp("Tiempo de la convolucion:"), disp(convTiempo)
