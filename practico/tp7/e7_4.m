#!/usr/bin/env octave-cli

graphics_toolkit("gnuplot");
pkg load signal;

function resultado = overlap_save(x, h)
	pos = 0;
	H = fft(h, 696776);
	M = max(size(h));
	overlap = M-1;
	N = 4*overlap;
	step = N-overlap;
	while pos+N <= length(x)
    		yt = ifft(fft(x(1+pos : N+pos), N).*H, N);
    		resultado(1+pos : step+pos) = yt(M : N);
    		pos = pos + step;
	end
endfunction

function resultado = overlap_add(x, h)
   N = 16;
   M = 4;
   L = 8;
   Nx = length(x);
   H = fft(h, N);
   i = 1;
   y = zeros(1, M + Nx - 1);
   while i <= Nx
       il = min(i + L - 1, Nx);
       yt = ifft( fft(x(i:il), N).* H, N);
       k  = min(i + N - 1, M + Nx - 1);
       y(i:k) = y(i:k) +  yt(1:k - i + 1).';
       i = i + L;
   end
   resultado = y;
endfunction

impulso = audioread("s1_r1_b_cd.wav");
h_l = impulso(:,1);
h_r = impulso(:,2);

[datos,sample] = audioread("Jahzzar_44100.wav");
x_l = datos(:,1);
x_r = datos(:,2);

tamh = numel(h_l)
tamx = numel(x_l)
tic
salida_l = overlap_save(x_l, h_l);
duracion = toc
salida_r = overlap_save(x_r, h_r);

audiowrite("audio_salaconcierto.wav", [salida_l,salida_r], sample);

%tic
%conv(x_l, h_l)
%duracion = toc
