#!/usr/bin/env octave-cli

L = 3
M = 2

pkg load signal;

[datos,sample] = audioread('Jahzzar_29400.wav');
player = audioplayer(datos(1:300000), 44100);
play(player);
pause;

datosUP = upsample(datos, L);

[b,a] = cheby1(8, 0.01, 1/L);
datosFiltrado = filter(b, a, datosUP);

datosDown = downsample(datosFiltrado, M);

datosFinal = datosDown;

player = audioplayer(datosFinal(1:400000), 44100);
play(player);
pause
audiowrite('Jahzzar_44100.wav', datosFinal, (sample*L/M));
