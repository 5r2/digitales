#!/usr/bin/env octave-cli

graphics_toolkit("gnuplot");
pkg load signal;

function factores = crear_factores_giro(cantidad)
	for x = 1:(cantidad/2)
		factores(x) = exp(-i*(2*pi*(x-1))/cantidad);
	endfor
endfunction

function [y0,y1] = mariposa(x0, x1, giro)
	y0 = x0 + (x1 * giro);
	y1 = x0 - (x1 * giro);
endfunction

function out_fft = mi_fft(dato)
	long = max(size(dato));
	x = bitrevorder(dato);
	giro = crear_factores_giro(long);
	for paso=2.^(0:log2(long/2))
		for j=0:paso-1
			for i=1:paso*2:long-j
				[x(i+j),x(i+j+paso)] = mariposa(x(i+j),x(i+j+paso),giro(j*(long/2)/paso+1));
			end
		end
	end
	out_fft = x;
endfunction

function out_fft = mi_fft_rapida(dato)
	long = max(size(dato));
	x = bitrevorder(dato);
	giro = crear_factores_giro(long);
	for etapa=log2(long/2):-1:1
		for i=1:(etapa^2)/2
			paso = 2^etapa;
			[x(i),x(i+paso)] = mariposa(x(i),x(i+paso),giro(i+paso/2));
		end
	end
	out_fft = x;
endfunction

%x = audioread("numeros.wav")(1:(2^12),1);
tam = 64;
x = complex(rand(1,tam),rand(1,tam));

tic; disp("3 fors");
mia_fft = mi_fft(x);
toc; tic; disp("2 fors");
ffft = mi_fft_rapida(x);
toc; tic; disp("nativo octave");
la_fft = fft(x);
toc

figure(1)
plot(abs(mia_fft/max(size(mia_fft))))
figure(2)
plot(abs(la_fft/max(size(la_fft))))
figure(3)
plot(abs(ffft/max(size(ffft))))
pause
