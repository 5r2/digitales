#!/usr/bin/env octave-cli

global graficar = 1;

pkg load signal;
if (graficar)
	graphics_toolkit("gnuplot");
	tamanio = get(0,"screensize");
	figure(1,"position", tamanio);
endif


function modulado = modular(datos, freqCos, sample)
	for n = 1:max(size(datos))
		modulado(n) = datos(n)*cos(2*pi*freqCos*n/sample);
	endfor
	%tam = max(size(datos))
	%inc = 2*pi/(tam-1)
	%micos = cos(0: inc : 2*pi);
	%modulado = datos .* micos;
endfunction

function graficarFFT(indice, datos, titulo)
	global graficar;
	if(graficar)
		subplot(5, 2, indice);
		plot(abs(fft(datos)(:,1))(1:end/2));
		title(titulo);
	endif
endfunction

function filtrado = filtrar(datos, freq, tipo)
	%[b,a] = cheby1(10, 0.1, freq, tipo);
	[b,a] = ellip(20, 1,120, freq, tipo);
	filtrado = filter(b, a, datos);
endfunction

disp("Leyendo audio");
[datos,sample] = audioread('numeros.wav');
graficarFFT(1, datos, "1 - Datos originales");
drawnow;

disp("Filtrando");
datosFiltrado = filtrar(datos, 0.45, "low");
graficarFFT(2, datosFiltrado, "2 - Datos filtrados");

disp("Modulando con coseno");
fCos = sample/4;
datosModulado = modular(datosFiltrado, fCos, sample);
graficarFFT(3, datosModulado.', "3 - Datos modulados");

disp("Filtrando");
datosModFiltrado = filtrar(datosModulado, 0.5, "high");
graficarFFT(4, datosModFiltrado.', "4 - Datos modulados post filtrado");

disp("Modulando con coseno");
datosGrave = modular(datosModFiltrado.', fCos+100, sample);
disp("Modulando con coseno");
datosAgudo = modular(datosModFiltrado.', fCos-100, sample);
disp("Modulando con coseno");
datosBase = modular(datosModFiltrado.', fCos, sample);

graficarFFT(5, datosGrave.', "5 - Datos remodulados, graves");
graficarFFT(6, datosAgudo.', "6 - Datos remodulados, agudos");
graficarFFT(7, datosBase.', "7 - Datos remodulados, original");

disp("Filtrando");
datosFinalGrave = filtrar(datosGrave, 0.5, "low");
disp("Filtrando");
datosFinalAgudo = filtrar(datosAgudo, 0.5, "low");
disp("Filtrando");
datosFinalBase = filtrar(datosBase, 0.5, "low");

graficarFFT(8, datosFinalGrave.', "8 - Datos remodulados filtrados, graves");
graficarFFT(9, datosFinalAgudo.', "9 - Datos remodulados filtrados, agudos");
graficarFFT(10, datosFinalBase.', "10 - Datos remodulados filtrados, original");

disp("Esscribiendo audio");
audiowrite('numerosModuladoGrave.wav', datosFinalGrave, sample);
disp("Esscribiendo audio");
audiowrite('numerosModuladoAgudo.wav', datosFinalAgudo, sample);
disp("Esscribiendo audio");
audiowrite('numerosModuladoBase.wav', datosFinalBase, sample);
disp("Presionar cualquier tecla para terminar");
pause;
