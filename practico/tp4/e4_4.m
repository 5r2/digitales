#!/usr/bin/env octave-cli
pkg load control;
pkg load geometry;
pkg load signal;

graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");
figure(1,"position", tamanio);

function coeffs = generarFiltro(fpass, fstop, ventana, anchoTransicion)
  switch(ventana)
    case "Rectangular"
      M = 4/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = rectwin(M);
    case "Hanning"
      M = 8/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = hann(M);
    case "Hamming"
      M = 8/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = hamming(M);
    case "Blackman"
      M = 12/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = blackman(M);
    otherwise
      M = 4/anchoTransicion;
      M = M+1;#Para cantidad impar de coeffs
      mVentana = rectwin(M);
  endswitch
  
  ft = (fpass+fstop)/2;
  
  for n = 1:M
    nciclo = n - (M-1)/2;
    hsinc(n) = ft*sinc(ft*(nciclo));
    hsinc(n) = hsinc(n) * mVentana(n);
  endfor

  coeffs = hsinc;
endfunction

function bincoeffs = cuantizarCoeficientes(bits, coeficientes)
  maxbpos = (2^(bits-1))-1;
  maxbneg = (2^(bits-1));
  hnorm = coeficientes./max(coeficientes);
  ptrneg = find(hnorm<0);
  hQ = hnorm .* maxbpos;
  hQ(ptrneg) = hnorm(ptrneg) .* maxbneg;
  hQ = round(hQ);
  bincoeffs = hQ;
endfunction

function respuestaDB(coeficientes, hz, nombre)
  muestras = 10000;
  H = fft(coeficientes, muestras);
  dBHpos = 20*log10(abs(H(1:muestras/2)));
  rango = [0:(1/(muestras/2-1))*hz:1*hz];
  grafico = plot(rango, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction

function invertirFiltro(coeficientes, hz, nombre)
  muestras = 10000;
  ceros = zeros(1,size(coeficientes)(2)/2-1);
  retardo = [ceros,1];
  H = fft(retardo, muestras) - fft(coeficientes, muestras);
  coeffs = H
  dBHpos = 20*log10(abs(H(1:muestras/2)));
  rango = [0:(1/(muestras/2-1))*hz:1*hz];
  grafico = plot(rango, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction

function coeffs = pasaBanda(fc1, fc2, B, ventana)
  pasaBajo = generarFiltro(fpass=fc2, fstop = fc2+B, ventana = ventana, anchoTransicion =B);
  pasaAlto = generarFiltro(fpass=1-fc1, fstop = 1-fc1-B, ventana = ventana, anchoTransicion =B);
  pasaAlto(1:2:end) *= -1;
  coeffs = conv(pasaAlto, pasaBajo);
endfunction

ej4p1 = pasaBanda(fc1=0.1, fc2=0.2, B=0.01, ventana = "Hamming");
respuestaDB(ej4p1, 1, "Pasa Banda");
pause;
