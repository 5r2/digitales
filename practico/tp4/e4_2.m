#!/usr/bin/env octave-cli
pkg load control;
pkg load geometry;
pkg load signal;

graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");
figure(1,"position", tamanio)

function coeffs = generarFiltro(fpass, fstop, ventana, anchoTransicion)
  switch(ventana)
    case "Rectangular"
      M = 4/anchoTransicion;
      mVentana = rectwin(M);
    case "Hanning"
      M = 8/anchoTransicion;
      mVentana = hann(M);
    case "Hamming"
      M = 8/anchoTransicion;
      mVentana = hamming(M);
    case "Blackman"
      M = 12/anchoTransicion;
      mVentana = blackman(M);
    otherwise
      M = 4/anchoTransicion;
      mVentana = rectwin(M);
  endswitch
  
  ft = (fpass+fstop)/2;
  
  for n = 1:M
    nciclo = n - (M-1)/2;
    hsinc(n) = ft*sinc(ft*(nciclo));
    hsinc(n) = hsinc(n) * mVentana(n);
  endfor

  coeffs = hsinc;
endfunction

function bincoeffs = cuantizarCoeficientes(bits, coeficientes)
  maxbpos = (2^(bits-1))-1;
  maxbneg = (2^(bits-1));
  hnorm = coeficientes./max(coeficientes);
  ptrneg = find(hnorm<0);
  hQ = hnorm .* maxbpos;
  hQ(ptrneg) = hnorm(ptrneg) .* maxbneg;
  hQ = round(hQ);
  bincoeffs = hQ;
endfunction

function respuestaDB(coeficientes, hz)
  H = fft(coeficientes, 1000);
  dBHpos = 20*log10(abs(H(1:500)));
  rango = [0:(1/(499))*hz:1*hz];
  grafico = plot(rango, dBHpos, 0.5,3);
endfunction

ej2p1 = generarFiltro(fpass=0.1, fstop=0.2, ventana="Rectangular", anchoTransicion=0.05);
stem(ej2p1);
pause;
respuestaDB(ej2p1, 1);
pause;

ej2p2 = generarFiltro(fpass=(3000/44100), fstop=(4000/44100), ventana="Hanning", anchoTransicion=0.04);
stem(ej2p2);
pause;
respuestaDB(ej2p2, 44100);
pause
