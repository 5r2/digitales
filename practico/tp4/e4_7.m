#!/usr/bin/env octave-cli
pkg load control;
pkg load geometry;
pkg load signal;

graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");
figure(1,"position", tamanio);

function respuestaDB(coeficientes, hz, nombre)
  muestras = 10000;
  H = fft(coeficientes, muestras);
  dBHpos = 20*log10(abs(H(1:muestras/2)));
  rango = [0:(1/(muestras/2-1)):1];
  grafico = plot(rango*hz/2, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction


function respuesta = iir(x,a,b,N,M)
  y = zeros(1, M+N-1);
  ene = zeros(1, N);
  regY = zeros(1, N);
  x = [x,zeros(1,N-1)];
  
  for n=1:(M+N-1)
    ene = [x(n), ene];
    ene(end) = [];
    for i=1:N
      y(n) = y(n) + ene(i)*b(i) - regY(i)*a(i+1);
    end
    regY = [y(n), regY];
    regY(end) = [];
  end
  respuesta = y;
end

sample = 256;
segundos = 8;
for t = 0:(1/sample):segundos
  f50(int32(t*sample+1)) = sin(2*pi*50*t);
  f100(int32(t*sample+1)) = sin(2*pi*100*t);
endfor
audio = f50+f100;

a = [1, -2.12984, 1.78256, -0.54343, 0];
b = [0.0528556, 0.0017905, 0.0017905, 0.0528556];
N = numel(b)
M = numel(audio)

respuesta = iir(x = audio, a = a, b = b, N = N, M=M);

#Ej2
respuestaDB(respuesta, 256, "Ej2");
pause;

#Ej 3
plot(respuesta);
hold on;
respuestaFilter = filter(b,a,audio);
plot(respuestaFilter);
hold off;
pause

[N, w] = freqz(b,a);
freq = sample* w / (2 * pi);
NdB = mag2db(abs(N));
plot(freq, NdB);
text(50, NdB(50*4), strcat("50 Hz: ",num2str(NdB(50*4))));
text(100, NdB(100*4), strcat("100 Hz: ",num2str(NdB(100*4))));
pause;
