#!/usr/bin/env octave-cli
pkg load control;
pkg load geometry;
pkg load signal;

graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");
figure(1,"position", tamanio);

function plotFreq(tiempo, sample)
  muestras = 10000;
  freqPos = muestras/2;
  H = fft(tiempo, muestras);
  dBHpos = 20*log10(abs(H(1:freqPos)));
  rango = [0:(1/(freqPos-1)):1];
  grafico = plot(rango*sample/2, dBHpos, 0.5,3);
endfunction


function ej5(b,a)
  #Ej 2
  imp = [1,zeros(1,255),zeros(1,256*3)];
  respuesta = filter(b,a,imp);
  plotFreq(respuesta, 256);
  
  respuesta2 = filter(b,a,imp);
  #Ej3
  db20hz = 0;
  i = 1;
  while db20hz > -120
    respuesta = filter(b,a,respuesta);
    i=i+1;
    plotFreq(respuesta, 256);
    db20hz = 20*(log10(abs(fft(respuesta,5120)(1:2560)))(2*200));
    disp([num2str(i), " filtrados, " num2str(db20hz), " atenuacion a 20hz"])
    pause(0.05);
  end
  pause;
  #Ej4
  
endfunction


#Ej 1
disp("Butter")
[b,a] = butter(3, 50/(256/2), 'high');
ej5(b=b,a=a)

#Ej5
disp("Cheby 1")
[b,a] = cheby1(3, 1, 50/(256/2), 'high');
ej5(b=b,a=a)
disp("Cheby 2")
[b,a] = cheby2(3, 10, 50/(256/2), 'high');
ej5(b=b,a=a)
disp("Ellip")
[b,a] = ellip(3, 1, 10, 50/(256/2), 'high');
ej5(b=b,a=a)