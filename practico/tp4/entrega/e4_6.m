#!/usr/bin/env octave-cli
pkg load control;
pkg load geometry;
pkg load signal;

graphics_toolkit('gnuplot');
tamanio = get(0,"screensize");
figure(1,"position", tamanio);

function respuestaDB(coeficientes, hz, nombre)
  muestras = 10000;
  H = fft(coeficientes, muestras);
  dBHpos = 10*log(abs(H(1:muestras/2)));
  rango = [0:(1/(muestras/2-1)):1];
  grafico = plot(rango*hz/2, dBHpos, 0.5,3);
  legend(grafico, nombre);
endfunction

sample = 21050;
segundos = 2;

for t = 0:(1/sample):segundos
  f1k(int32(t*sample+1)) = sin(2*pi*1000*t);
  f1k1(int32(t*sample+1)) = sin(2*pi*1100*t);
endfor
audio = f1k+f1k1;
sound(audio, sample);

freq = [0, 1000/(21050/2), 1100/(21050/2), 1];
ampl = [1, 1, 0, 0];
LMS = firls(350,freq, ampl);
RMZ = remez(350, freq, ampl);

respuestaDB(LMS, 21050, "LMS");
hold on;
respuestaDB(RMZ, 21050, "Remez");
pause;

filtrado = conv(RMZ, audio);
sound(filtrado, sample);