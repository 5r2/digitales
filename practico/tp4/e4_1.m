#!/usr/bin/env octave-cli
pkg load control;
pkg load geometry;
pkg load signal;
graphics_toolkit('gnuplot');

muestras = 1000;
freqPos = muestras/2;

h = load("low_pass.dat");
tamanio = get(0,"screensize");
figure(1,"position", tamanio)
grafico = stem(h);
set(grafico, "linewidth", 3);
pause();

H = fft(h, muestras);

dBHpos = 20*log10(abs(H(1:freqPos)));

rango = [0:(1/(freqPos-1)):1];
grafico = plot(rango, dBHpos, 0.5,3);
set(grafico, "linewidth", 3);
xlabel("Frequencia [FSample/2]");
ylabel("Ganancia [dB]");
title("Respuesta en frecuencia del filtro");

bandaPaso = dBHpos<(-3);
corte = find(bandaPaso, 1);

hold on;
plot(rango(corte), dBHpos(corte), 'ro');
text(rango(corte+5), dBHpos(corte), strcat("Frecuencia de corte: ",num2str(corte)));

[maxBP posMax] = max(dBHpos(1:corte));
[minBP posMin] = min(dBHpos(1:posMax));

vRango = [1:1:freqPos];
vMax(vRango) = maxBP;
vMin(vRango) = minBP;

plot(rango, vMax);
plot(rango, vMin);

ripple = maxBP-minBP;
text(1, (maxBP+minBP)/2, strcat("Ripple: ", num2str(ripple), "dB"))

plot(0,dBHpos(1), 'ro');
text(0,dBHpos(1)+1, strcat("Ganancia en continua: ", num2str(dBHpos(1)), "dB"));

[picos pospicos] = findpeaks(dBHpos(corte:end), "DoubleSided")
posSba = pospicos(2)+corte-1
valSba = picos(2)
plot(rango(posSba), valSba, 'ro')

x = rango(posSba)
max = (maxBP+minBP)/2
min = valSba
plot([x,x],[min,max])
text(rango(posSba), (valSba+max)/2, strcat("Atenuacion banda de rechazo: ", num2str(max-valSba), "dB"))

pause();
